# BlazorRPG

This is an RPG made in Blazor.

If someone has opium to help me get through this, that would be very much appreciated.

## Very rough DB mockup
- Player
- - Id
- - Username
- - Email
- - Password
- - Gold
- Stats
- - Strength
- - Speed
- - Dexterity
- - Defense
- - Perseption
- - Health
- - Experience
- - UserId
- Weapon
- - Id
- - Name
- - DamageMod
- - Cost
- - Space
- Armour
- - Id
- - Name
- - DefenseMod
- - Cost
- - Space
- Gear
- - WeaponId
- - ArmourId
- - UserId
- Inventory
- - Id
- - MaxSpace
- - SpaceLeft
- Chat
- - Id
- - UserId1
- - UserId2
- - Name
- Message
- - Id
- - ChatID
- - Text

## Requirements
- "Login"
- Chat
- Attacking
- - Mugging
- Training
- Shop
- Inventory
